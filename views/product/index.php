<?php require 'views/header.php'; ?>
<main>
    <div>

        <h1>Lista de productos</h1>

        <table>
            <tr>
                <th>Id</th>
                <th>Tipo_id</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Fecha</th>

                <th>Acciones</th>
            </tr>
            <?php foreach ($products as $product): ?>
                <tr>
                    <td><?php
                    if ($product->id == 1){
                            echo 'Perifericos';
                    }
                    else if ($product->id == 2) {
                            echo 'Almacenamiento';
                    }
                    else if ($product->id == 3) {
                            echo 'Consumibles';
                    }else{
                        echo $product->id;
                    }




                     ?>
                    </td>
                    <td><?php echo $product->id_tipo ?>
                    </td>
                    <td><?php echo $product->nombre ?></td>
                    <td><?php echo $product->precio ?></td>
                    <td><?php echo $product->fecha ?></td>
                    <td>
                        <a href="<?php echo "delete/$product->id" ?>">borrar</a>
                        -
                        <a href="<?php
                        echo "edit/$product->id"
                        ?>">editar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>

        <p>
        <a href="create">Nuevo</a>
        </p>
    </div>

</main>

<?php require 'views/footer.php'; ?>
