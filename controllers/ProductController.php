<?php

require_once 'models/Product.php';

class ProductController
{

    function __construct()
    {
        # code...
    }

    public function index(){
        $products = Product::all();
        require 'views/product/index.php';
    }

    public function create()
    {
        require('views/product/create.php');
    }

    public function store()
    {

        $product = new Product;

        $product->id = $_POST['id'];
        $product->id_tipo = $_POST['id_tipo'];
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];

        $product->fecha = $_POST['fecha'];
        $product->store();
        header('location:index');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        include 'views/product/edit.php';
    }

     public function update($id)
    {

        $product = Product::find($id);

        $product->id = $_POST['id'];
        $product->id_tipo = $_POST['id_tipo'];
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = $_POST['fecha'];

        $product->save();

        $_SESSION['common'] =  array();
        array_push($_SESSION['common'], $product);

        header('location:../index');
    }



}


?>
