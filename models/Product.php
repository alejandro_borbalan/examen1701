<?php

require('app/Model.php');

/**
*
*/
class Product extends Model
{
    public $id;
    public $id_tipo;
    public $nombre;
    public $precio;
    public $fecha;

    function __construct()
    {
        # code...
    }

    public static function all(){

        $db = Product::connect();

        $stmt = $db->prepare("SELECT * FROM producto");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');

        $results = $stmt->fetchAll();

            return $results;
        }

    public function store(){

        $db = $this->connect();
        $sql = "INSERT INTO producto( id, id_tipo,nombre,precio,fecha) VALUES(?, ?, ?, ?, ?)";

        $stmt = $db->prepare($sql);

        $stmt->bindParam(1, $this->id);
        $stmt->bindParam(2, $this->id_tipo);
        $stmt->bindParam(3, $this->nombre);
        $stmt->bindParam(4, $this->precio);
        $stmt->bindParam(5, $this->fecha);

        $result = $stmt->execute();

        return $result;
    }

    public function find($id)
    {
        $db = Product::connect();
        $sql = "SELECT * FROM producto WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');
        $result = $stmt->fetch();
        return $result;
    }


    public function save()
    {
        $db = $this->connect();
        $sql = "UPDATE producto SET id=?, id_tipo=?, nombre=?, precio=?,fecha=? WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->id);
        $stmt->bindParam(2, $this->id_tipo);
        $stmt->bindParam(3, $this->nombre);
        $stmt->bindParam(4, $this->precio);
        $stmt->bindParam(5, $this->fecha);
        $stmt->bindParam(6, $this->id);

        $result = $stmt->execute();
        return $result;
    }
}
?>
